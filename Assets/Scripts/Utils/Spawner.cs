using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] objectPool;

    public Transform PlayerForwardReference;

    public void InitializePool()
    {
        foreach (GameObject obj in objectPool)
        {
            obj.SetActive(false);
        }
    }

    public void Spawn(Transform[] spawnPoints, int objectsNumber = 1)
    {
        int spawnNumber = 0;
        Shuffle(objectPool);
        foreach (GameObject obj in objectPool)
        {
            Vector3 newBotPos = PlayerForwardReference.position;;
            newBotPos.y = 3;
            newBotPos.z =  newBotPos.z + Random.Range(-10, 10);
            newBotPos.x =  newBotPos.x + Random.Range(-10, 10);
            GameObject newObj = Instantiate(obj, newBotPos, Quaternion.identity);
            newObj.SetActive(true);
            spawnNumber++;
            if (spawnNumber >= objectsNumber) return;
        }
    }

    private void Shuffle(GameObject[] objects)
    {
        for (int i = 0; i < objects.Length; i++)
        {
            GameObject temp = objects[i];
            int randomIndex = Random.Range(i, objects.Length);
            objects[i] = objects[randomIndex];
            objects[randomIndex] = temp;
        }
    }
}
