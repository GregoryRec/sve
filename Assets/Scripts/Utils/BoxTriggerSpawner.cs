using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class BoxTriggerSpawner : Spawner
{
    [SerializeField]
    private string triggerTag;

    [SerializeField]
    private Transform[] spawnPoints;

    [SerializeField]
    private int objectsPerSpawn = 1;

    private bool spawned = false;


    [SerializeField]
    private bool oneTime = true;
    private void Awake()
    {
        InitializePool();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (oneTime && spawned) return;

        if (other.gameObject.tag == triggerTag)
        {
            Spawn(spawnPoints, objectsPerSpawn);
            spawned = true;
        }
    }
}
