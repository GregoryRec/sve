﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
     public bool impactada = false;
    /*public GameObject cementoHole;
    public GameObject metalHole;
    public GameObject woodHole;*/
    public GameObject bloodEffect;

    [SerializeField]
    private float damage;
    private void Start()
    {
        Physics.IgnoreLayerCollision(2, 11, true);
        impactada = false;
    }
    void OnCollisionEnter(Collision other)
    {
        Rigidbody otherRigidbody = other.gameObject.GetComponent<Rigidbody>();
        if (otherRigidbody)
        {
            otherRigidbody.AddForce(gameObject.transform.forward * gameObject.GetComponent<Rigidbody>().velocity.magnitude);
        }
        Vector3 effectPosition = other.contacts[0].point;
        effectPosition.z = effectPosition.z + 0.002f;

        /*
        if (other.collider.tag == "cemento" && !impactada)
        {
            GameObject newImpact = Instantiate(cementoHole, effectPosition, Quaternion.LookRotation(other.contacts[0].normal));
            Transform[] allChildren = other.gameObject.GetComponentsInChildren<Transform>();
            foreach (Transform child in allChildren)
            {
                if (child.gameObject.name == "BulletEffects") newImpact.transform.SetParent(child);
            }
        impactada = true;
        }
        if (other.collider.tag == "targetHitbox" && !impactada)
        {
            //other.gameObject.GetComponent<targetHitbox>().Dead();
            GameObject newImpact = Instantiate(woodHole, effectPosition, Quaternion.LookRotation(other.contacts[0].normal));
            Transform[] allChildren = other.gameObject.GetComponentsInChildren<Transform>();
            foreach (Transform child in allChildren)
            {
                if (child.gameObject.name == "BulletEffects") newImpact.transform.SetParent(child);
            }
            impactada = true;
            //Simulation.enElBlanco++;
            Destroy(this);
        }
        if (other.collider.tag == "madera" && !impactada)
            {
                GameObject newImpact = Instantiate(woodHole, effectPosition, Quaternion.LookRotation(other.contacts[0].normal));
                Transform[] allChildren = other.gameObject.GetComponentsInChildren<Transform>();
                foreach (Transform child in allChildren)
                {
                    if (child.gameObject.name == "BulletEffects") newImpact.transform.SetParent(child);
                }
                impactada = true;
                Destroy(this);
            }
            if (other.collider.tag == "metal" && !impactada)
            {
                GameObject newImpact = Instantiate(metalHole, effectPosition, Quaternion.LookRotation(other.contacts[0].normal));
                Transform[] allChildren = other.gameObject.GetComponentsInChildren<Transform>();
                foreach (Transform child in allChildren)
                {
                    if (child.gameObject.name == "BulletEffects") newImpact.transform.SetParent(child);
                }
                impactada= true;
            }
           

        if (other.collider.CompareTag("Player") && !impactada)
        {
            GameObject newImpact = Instantiate(bloodEffect, effectPosition, Quaternion.LookRotation(other.contacts[0].normal));
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            impactada = true;
            Debug.Log("OHNO ME DIERON GA");
            //Simulation.disparosRecibidos++;
        }


        if (other.collider.CompareTag("HostageHitbox") && !impactada)
        {
           other.collider.GetComponent<HostageHitbox>().SetDamage();
           Instantiate(bloodEffect, other.contacts[0].point, Quaternion.LookRotation(other.contacts[0].normal), null);
           GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            impactada = true;
        }
        */
        if (other.collider.CompareTag("HostileHitbox") && !impactada)
        {
            HostileHitbox hostileHitbox = other.gameObject.GetComponent<HostileHitbox>();
            Debug.Log("Disparo impactado");
            if (hostileHitbox)
            {
                hostileHitbox.SetDamage(damage);
            }
            /*
            GameObject newImpact = Instantiate(bloodEffect, effectPosition, Quaternion.LookRotation(other.contacts[0].normal));
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            Transform[] allChildren = other.gameObject.GetComponentsInChildren<Transform>();
            foreach (Transform child in allChildren)
            {
                if (child.gameObject.name == "BulletEffects") newImpact.transform.SetParent(child);
            }*/
            impactada = true;
        }
    }
}
