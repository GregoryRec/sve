using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Galil : MonoBehaviour
{
    public Weapon weapon;
    public OVRInput.Button shootingButton;
    public bool hasEnemy = false;
    public string weaponName;
    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(shootingButton) || Input.GetMouseButtonDown(0))
        {
            weapon.TriggerShoot();
        }

        if (hasEnemy) Debug.DrawRay(transform.position, transform.forward * 50, Color.red);
        else Debug.DrawRay(transform.position, transform.forward * 50, Color.green);

    }
}
