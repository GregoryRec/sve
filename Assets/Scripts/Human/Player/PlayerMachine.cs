using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMachine : MonoBehaviour
{

    public Transform playerColliderTransform;
    Vector3 vectorRotation;
    
    void Update()
    {
        vectorRotation = playerColliderTransform.eulerAngles;
        transform.rotation = Quaternion.Euler(0f, vectorRotation.y, 0f);
        transform.position = playerColliderTransform.position;
    }
}
