using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HumanStats : MonoBehaviour
{
    [SerializeField]
    private float life;
    [SerializeField]
    private string playerName;

    
    [SerializeField]
    Slider lifeBar;

    [SerializeField]
    GameObject lifeBarFill;
    public float GetLife()
    {
        return life;
    }

    public void SetLife(float newLife)
    {
        life = newLife;
        if(lifeBar != null) lifeBar.value = life;
    }

    public string GetPlayerName()
    {
        return playerName;
    }

    public void SetPlayerName(string newPlayerName)
    {
        playerName = newPlayerName;
    }

    public bool IsDead()
    {
        if(transform.position.y < -10) life = 0;
        if (life <= 0) {
            if(lifeBarFill != null) lifeBarFill.SetActive(false);
            return true;
        } 
        else return false;
    }

}
