using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotBehavior : MonoBehaviour
{
    [SerializeField]
    private string BehaviorName;

    private BotMachine botMachine;

    private Animator animator;

    public bool CanDeactivate = true;

    private GameObject target;

    [SerializeField]
    public float offsetRotationY;
    [SerializeField]
    public float rotationSpeed;

    public string GetBehaviorName()
    {
        return BehaviorName;
    }

    public void SetBotMachine(BotMachine botMachine)
    {
        this.botMachine = botMachine;
    }

    public BotMachine GetBotMachine()
    {
        return botMachine;
    }

    public GameObject GetTarget()
    {
        return target;
    }

    public void SetTarget(GameObject target)
    {
        this.target = target;
    }

    public void SetAnimator(Animator animator)
    {
        this.animator = animator;
    }

    public Animator GetAnimator()
    {
        return animator;
    }
}
