using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostilePatrolBehavior : BotBehavior
{
    [Tooltip("Para generar el recorrido del both")]
    [SerializeField]
    private List<Vector3> patrolPointsDistance = new List<Vector3>();



    private List<Vector3> patrolPoints = new List<Vector3>();

    private int nextWayPoint;

    [Tooltip("Rapidez del bot para patrulla")]
    [SerializeField]
    private float speed = 5;

    [Tooltip("Offset para patrullar")]
    [SerializeField]
    private float stopDistance = 1;

    [SerializeField]
    private string walkAimingTriggerName;

    private void OnEnable()
    {
        Initialize();
    }

    void Initialize()
    {
        GeneratePath();
        //UpdateDestine();
        //GetBotMachine().SetStoppingDistance(stopDistance); // Debe llegar a cada punto para que se detenga
        //GetBotMachine().SetNavMeshAgentSpeed(speed);
        GetAnimator().SetTrigger(walkAimingTriggerName);
        //GetBotMachine().SetRotationSpeed(rotationSpeed);
        GetBotMachine().SetRotationModelOffsetY(offsetRotationY);
    }


    // Update is called once per frame
    void Update()
    {
        //if (patrolPoints.Count == 0 || patrolPoints == null) return;
        // if (GetBotMachine().IsArrived())
        // {
        //     nextWayPoint = (nextWayPoint + 1) % patrolPoints.Count;
        //     UpdateDestine();
        // }

        var step =  speed * Time.deltaTime; // calculate distance to move
        transform.position = Vector3.MoveTowards(transform.position, GetBotMachine().GetTarget().transform.position, step);

        
        RaycastHit hit;
        if (GetComponent<HostileVisionBehavior>().TargetOnRange(out hit))
        {
            Debug.Log("Objetivo en la mira");
            GetBotMachine().SetEnabledBehavior(GetComponent<HostileAttackBehavior>(), true);
        }

        
    }
    public void UpdateDestine()
    {
        GetBotMachine().UpdateNavMeshAgentDestinePoint(patrolPoints[nextWayPoint]);

        GetBotMachine().LookTarget(patrolPoints[nextWayPoint]);
    }

    void GeneratePath()
    {
        patrolPoints.RemoveRange(0, patrolPoints.Count);
        foreach(Vector3 patrolPointDistance in patrolPointsDistance)
        {
            patrolPoints.Add(new Vector3(transform.position.x + patrolPointDistance.x, transform.position.y + patrolPointDistance.y, transform.position.z + patrolPointDistance.z));
        }
    }
    void OnDrawGizmosSelected()
    {

        foreach(Vector3 patrolPoint in patrolPoints)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(patrolPoint, 0.1f);
        }

        foreach (Vector3 patrolPointDistance in patrolPointsDistance)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawSphere(new Vector3(transform.position.x + patrolPointDistance.x, transform.position.y + patrolPointDistance.y, transform.position.z + patrolPointDistance.z), 0.1f);
        }
    }
}
