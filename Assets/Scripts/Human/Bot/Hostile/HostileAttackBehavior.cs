using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileAttackBehavior : BotBehavior
{

    [Tooltip("Distancia m�nima para que el bot pueda disparar")]
    [SerializeField]
    float distanceAttack;


    [Tooltip("Tiempo de espera cuando el player no est� en la mira para volver a modo patrulla")]
    [SerializeField]
    float waitTime = 5;

    float timeElapsed = 0;

    float timeElapsedWaitTime = 0;

    [SerializeField]
    private string shotTriggerName;

    [Tooltip("Tiempo entre cada disparo, en segundos")]
    [SerializeField]
    private float shotTime = 3;

    [SerializeField]
    private string aimTriggerName;

    bool aiming = true;
    private void OnEnable()
    {
        Initialize();
    }
    void Initialize()
    {
        // GetBotMachine().SetStoppingDistance(distanceAttack); // Distancia a la que se detiene el bot para atacar
        // GetBotMachine().UpdateNavMeshAgentDestinePoint(GetTarget().transform.position);
        // GetBotMachine().SetRotationSpeed(rotationSpeed);
        GetBotMachine().SetRotationModelOffsetY(offsetRotationY);

    }
    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (!GetComponent<HostileVisionBehavior>().TargetOnRange(out hit))
        {
            Debug.Log("Fuera de rango");
            GetAnimator().SetTrigger(aimTriggerName);
            timeElapsedWaitTime += Time.deltaTime;
            if (timeElapsedWaitTime >= waitTime)
            {
                GetBotMachine().SetEnabledBehavior(GetComponent<HostilePatrolBehavior>(), true);
                timeElapsedWaitTime = 0;
                return;
            }
        }
        else
        {
            GetBotMachine().LookTarget(GetTarget().transform.position);
            timeElapsed += Time.deltaTime;
            if (GetBotMachine().IsArrived())
            {
                GetAnimator().SetTrigger(aimTriggerName);
                if (timeElapsed >= shotTime)
                {
                    Debug.Log("Shot");
                    GetAnimator().SetTrigger(shotTriggerName);
                    timeElapsed = 0;
                    aiming = false;
                }
            }
        }
    }

    public void Aiming()
    {
        aiming = true;
    }
}
