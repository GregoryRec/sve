using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileHitbox : BotBehavior
{
    
    public void SetDamage(float damage)
    {
        GetBotMachine().SetDamage(damage);
    }
}

