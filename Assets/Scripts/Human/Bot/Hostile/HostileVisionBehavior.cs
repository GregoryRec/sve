using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileVisionBehavior : BotBehavior
{
    [SerializeField]
    Transform eyes;
    public float visionRange;
    public LayerMask ignoreLayer;
    public Vector3 offset = new Vector3(0f, 0.75f, 0f);

    public bool TargetOnRange(out RaycastHit hit)
    {
        Vector3 direction;

        if (GetTarget())
        {
            direction = (GetTarget().transform.position + offset) - eyes.position;
        }
        else
        {
            direction = eyes.forward;
        }

        Debug.DrawRay(eyes.position, direction, Color.red, 0.1f);


        if (Physics.Raycast(eyes.position, direction, out hit, visionRange, ~ignoreLayer))
        {
            Debug.DrawLine(eyes.position, hit.collider.gameObject.transform.position, Color.blue, 0.1f);
            Debug.Log("hit.collider: " + hit.collider.tag);
            return hit.collider.CompareTag(GetTarget().tag);
        }
        return false;
    }
}
