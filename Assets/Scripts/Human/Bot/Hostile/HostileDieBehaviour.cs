using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileDieBehaviour : BotBehavior
{
    [SerializeField]
    private string dieTriggerName;

    private Collider collider;

    private void Awake()
    {
        collider = GetComponent<CapsuleCollider>();
    }
    private void OnEnable()
    {
        GetAnimator().SetTrigger(dieTriggerName);
        //GetBotMachine().GetNavMeshAgent().enabled = false;
        collider.enabled = false;
        Debug.Log("Muriendo");
        Destroy(this.gameObject);
    }
}
