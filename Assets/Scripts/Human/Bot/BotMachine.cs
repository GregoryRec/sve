using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class BotMachine : HumanStats
{
    [SerializeField]
    private List<BotBehavior> botBehaviors;

    [SerializeField]
    private BotBehavior initBehavior;

    [SerializeField]
    private GameObject target;

    [SerializeField]
    private NavMeshAgent navMeshAgent;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private Transform modelTransform;

    [SerializeField]
    private float rotationSpeed = 3;
    private void Awake()
    {
        Initialized(GetPlayerName(),GetLife());
    }
    public void Initialized(string playerName, float life)
    {
        SetLife(life);
        SetPlayerName(playerName);
        InitializeBotBehaviours();
    }

    void InitializeBotBehaviours()
    {
        foreach (BotBehavior behavior in botBehaviors)
        {
            behavior.SetBotMachine(this);
            behavior.SetTarget(target);
            behavior.SetAnimator(animator);
        }
        ShutDownAllBehaviors();
        SetEnabledBehavior(initBehavior, true);
    }

    public void SetRotationSpeed(float rotationSpeed)
    {
        this.rotationSpeed = rotationSpeed;
    }
    public void ShutDownAllBehaviors()
    {
        foreach(BotBehavior behavior in botBehaviors)
        {
            if(behavior.CanDeactivate) behavior.enabled = false;
        }
        ResetAllTriggers();
    }
    public void ResetAllTriggers()
    {
        foreach (var param in animator.parameters)
        {
            if (param.type == AnimatorControllerParameterType.Trigger)
            {
                animator.ResetTrigger(param.name);
            }
        }
    }
    public void SetDamage(float damage)
    {
        SetLife(this.GetLife() - damage);
    }

    public void AddBehavior(BotBehavior botBehavior)
    {
        botBehaviors.Add(botBehavior);
    }

    public void SetEnabledBehavior(BotBehavior botBehavior, bool state)
    {
        ShutDownAllBehaviors();
        botBehaviors.Find(behavior => behavior.GetBehaviorName() == botBehavior.GetBehaviorName()).enabled = state;
    }

    public void SetTarget(GameObject target)
    {
        this.target = target;
    }

    public void UpdateNavMeshAgentDestinePoint(Vector3 destinePoint)
    {
        navMeshAgent.destination = destinePoint;
    }

    public void SetStoppingDistance(float stoppingDistance)
    {
        navMeshAgent.stoppingDistance = stoppingDistance;
    }
    public void SetNavMeshAgentSpeed(float speed)
    {
        navMeshAgent.speed = speed;
    }

    public NavMeshAgent GetNavMeshAgent()
    {
        return navMeshAgent;
    }

    public bool IsArrived()
    {
        return Vector2.Distance(transform.position,target.transform.position)>2;
        //return navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance && !navMeshAgent.pathPending;
    }

    public GameObject GetTarget()
    {
        return target;
    }

    public void SetRotationModelOffsetY(float y)
    {
        modelTransform.localRotation = Quaternion.Euler(0, y, 0);
    }
    
    public void LookTarget(Vector3 targePosition)
    {
        Quaternion targetRotation = Quaternion.LookRotation(targePosition - transform.position);
        targetRotation.x = 0;
        targetRotation.z = 0;
        // Smoothly rotate towards the target point.
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }


    private void Update()
    {
        if(IsDead())
        {
            SetEnabledBehavior(GetComponent<HostileDieBehaviour>(), true);
        }
    }

}
